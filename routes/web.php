<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (session()->has('link')) {
        return redirect(session('link'));
    }
    return view('welcome');
});

Route::group(['prefix' => 'user'], function (){
    Route::get('/formPersonal','UsersController@formPersonal');
    Route::get('/formAddress','UsersController@formAddress');
    Route::get('/formPayment','UsersController@formPayment');
    Route::get('/success/{id}','UsersController@success');
    Route::post('/savePersonal','UsersController@savePersonal');
    Route::post('/saveAddress','UsersController@saveAddress');
    Route::post('/savePayment','UsersController@savePayment');
});
