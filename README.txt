SET UP

Be sure you're in the branch 'master'
Run the following command: sudo docker-compose up --build -d
Then access the link http://localhost:8080/

QUESTIONS

1. Describe possible performance optimizations for your Code.
- Could add a service layer to perform database operations.
- Do a javascript code to save the data as the user type.


2. Which things could be done better, than you’ve done it?
- The Dockerfile could be refactored
- Could have used more the blade structures
- Could have handled errors better
- Make more and better tests
