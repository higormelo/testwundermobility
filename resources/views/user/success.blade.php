<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Wunder Mobility</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Success
                </div>

                <div class="row">
                    <p>Name: {{$user->firstname . ' ' . $user->lastname}}</p>
                    <p>Telephone: {{$user->telephone}}</p>
                    <p>Address: {{$user->address->street . ', ' . $user->address->house_number . ' - ' . $user->address->city}}</p>
                    <p>Zip Code: {{$user->address->zip_code}}</p>
                    <p>Account Owner: {{$user->payment->account_owner}}</p>
                    <p>IBAN: {{$user->payment->iban}}</p>
                    <p>Payment Data Id: {{$user->payment->payment_data_id}}</p>
                </div>
            </div>
        </div>
    </body>
</html>
