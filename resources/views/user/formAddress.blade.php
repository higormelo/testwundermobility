<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wunder Mobility</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

</head>
<body>
    <div class="content">
        <div class="header">
            <h2>Address Information</h2>
        </div>
        @if(Session::has('errors'))
            <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('errors')->first() }}</div>
        @endif
        <form id="model_form" method="post" action="{{url('user/saveAddress')}}" class="form-horizontal">
            @csrf
            <div class="row">
                <label for="street">Street:</label>
                <input type="text" name="street" id="street" required value="{{($address) ? $address['street'] : ''}}">
            </div>
            <div class="row">
                <label for="house_number">House Number:</label>
                <input type="number" name="house_number" id="house_number" required value="{{($address) ? $address['house_number'] : ''}}">
            </div>
            <div class="row">
                <label for="zip_code">Zip Code:</label>
                <input type="text" name="zip_code" id="zip_code" required value="{{($address) ? $address['zip_code'] : ''}}">
            </div>
            <div class="row">
                <label for="city">City:</label>
                <input type="text" name="city" id="city" required value="{{($address) ? $address['city'] : ''}}">
            </div>
            <div class="row">
                <a href="{{url('user/formPersonal')}}">
                    <input type="button" value="Last Step" />
                </a>
                <input type="submit" value="Next Step">
            </div>
        </form>
    </div>
</body>
</html>
