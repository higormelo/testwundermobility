<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wunder Mobility</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

</head>
<body>
    <div class="content">
        <div class="header">
            <h2>Payment Information</h2>
        </div>
        @if(Session::has('errors'))
            <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('errors')->first() }}</div>
        @endif
        <form id="model_form" method="post" action="{{url('user/savePayment')}}" class="form-horizontal">
            @csrf
            <div class="row">
                <label for="account_owner">Account Owner:</label>
                <input type="text" name="account_owner" id="account_owner" required value="{{($payment) ? $payment['account_owner'] : ''}}">
            </div>
            <div class="row">
                <label for="iban">IBAN:</label>
                <input type="text" name="iban" id="iban" required value="{{($payment) ? $payment['iban'] : ''}}">
            </div>
            <div class="row">
                <a href="{{url('user/formAddress')}}">
                    <input type="button" value="Last Step" />
                </a>
                <input type="submit" value="Next Step">
            </div>
        </form>
    </div>
</body>
</html>
