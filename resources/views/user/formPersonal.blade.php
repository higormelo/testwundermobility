<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wunder Mobility</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

</head>
<body>
    <div class="content">
        <div class="header">
            <h2>Personal Information</h2>
        </div>
        @if(Session::has('errors'))
            <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('errors')->first() }}</div>
        @endif
        <form id="model_form" method="post" action="{{url('user/savePersonal')}}" class="form-horizontal">
            @csrf
            <div class="row">
                <label for="firstname">First Name:</label>
                <input type="text" name="firstname" id="firstname" required value="{{($user) ? $user['firstname'] : ''}}">
            </div>
            <div class="row">
                <label for="lastname">Last Name:</label>
                <input type="text" name="lastname" id="lastname" required value="{{($user) ? $user['lastname'] : ''}}">
            </div>
            <div class="row">
                <label for="telephone">Telephone:</label>
                <input type="text" name="telephone" id="telephone" required value="{{($user) ? $user['telephone'] : ''}}">
            </div>
            <div class="row">
                <input type="submit" value="Next Step">
            </div>
        </form>
    </div>
</body>
</html>
