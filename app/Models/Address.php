<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $table = 'address';

    public $timestamps = false;

    protected $fillable = [
        'street',
        'house_number',
        'zip_code',
        'city',
        'user_id',
        'created_at',
        'updated_at'
    ];
}
