<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $table = 'payments';

    public $timestamps = false;

    protected $fillable = [
        'account_owner',
        'iban',
        'payment_data_id',
        'user_id',
        'created_at',
        'updated_at'
    ];
}
