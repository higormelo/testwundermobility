<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Http;

class UsersController extends Controller
{
    public function formPersonal(Request $request)
    {
        try {
            $data = $request->session()->all();
            // check if there is data in session
            $user = (isset($data['user'])) ? $data['user'] : false;
            return view('user.formPersonal', ['user' => $user]);
        } catch (\Exception $exception) {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function savePersonal(Request $request)
    {
        try {
            $data = $request->all();
            unset($data['_token']);
            $data = ['user' => $data];
            // save data in the session - key: user
            $request->session()->put($data);
            session(['link' => url()->previous()]);
            return redirect(url('user/formAddress'));
        } catch (Exception $exception) {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function formAddress(Request $request)
    {
        try {
            $data = $request->session()->all();
            // check if there is data in session
            $address = (isset($data['address'])) ? $data['address'] : false;
            return view('user.formAddress', ['address' => $address]);
        } catch (\Exception $exception) {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function saveAddress(Request $request)
    {
        try {
            $data = $request->all();
            unset($data['_token']);
            $data = ['address' => $data];
            // save data in the session - key: address
            $request->session()->put($data);
            session(['link' => url()->previous()]);
            return redirect(url('user/formPayment'));
        } catch (Exception $exception) {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function formPayment(Request $request)
    {
        try {
            $data = $request->session()->all();
            // check if there is data in session
            $payment = (isset($data['payment'])) ? $data['payment'] : false;
            return view('user.formPayment', ['payment' => $payment]);
        } catch (\Exception $exception) {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function savePayment(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->all();
            unset($data['_token']);
            $session = $request->session()->all();
            // save user data
            $session['user']['created_at'] = date('Y-m-d H:i:s');
            $user = User::create($session['user']);
            // save address data
            $session['address']['created_at'] = date('Y-m-d H:i:s');
            $session['address']['user_id'] = $user->id;
            $address = Address::create($session['address']);
            // access API
            $response = Http::post('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                'customerId' => $user->id,
                'iban' => $data['iban'],
                'owner' => $data['account_owner']
            ]);
            if (!$response->successful()) { // check response
                throw new Exception($response->json());
            }
            $data['payment_data_id'] = $response->json()['paymentDataId'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['user_id'] = $user->id;
            // save payment data
            $payment = Payment::create($data);
            // update user data
            $user->update(['payment_id' => $payment->id]);
            // erase data in session
            $request->session()->forget(['user', 'address', 'link']);
            DB::commit();
            return redirect(url('user/success/' . $user->id));
        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    public function success($id)
    {
        try {
            $user = User::find($id);
            return view('user.success', ['user' => $user]);
        } catch (Exception $exception) {
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

    }
}
