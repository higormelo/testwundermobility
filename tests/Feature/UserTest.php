<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function homepageTest()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /** @test */
    public function savePersonalTest()
    {
        $response = $this->post('/savePersonal', [
            'firstname' => 'FirstName',
            'lastname' => 'LastName',
            'telephone' => '999999999'
        ]);
        $response->assertStatus(200);
        $response->assertSessionHas(['firstname', 'lastname', 'telephone']);
    }

    /** @test */
    public function saveAddressTest()
    {
        $response = $this->post('/saveAddress', [
            'street' => 'street',
            'house_number' => 10,
            'zip_code' => '999999999',
            'city' => 'city'
        ]);
        $response->assertStatus(200);
        $response->assertSessionHas(['firstname', 'lastname', 'telephone']);
    }
}
